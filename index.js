let trainer = {
    name: 'Ash Ketchun',
    age: 10,
    Pokemon: ['pikachu', 'bulbasaur', 'eevee', 'Snorlax' ],
    friends: {hoenn: ["May", "Max"],
              kanto:  ['Brock', 'Misty'] 
                },
    talk: function(){
    console.log('Pikachu! I choose you!');

    }


}

console.log(trainer);

console.log('Result of dot notation: ');
console.log(trainer.name);

console.log('Result of dot notation: ');
console.log(trainer['Pokemon']);

console.log('Result of talk method');
trainer.talk();

function Pokemon(name, level){

    // Propertiees 
    this.name = name;
    this.level = level;
    this.health = 2* level; // 4 health
    this.attack = level;

    // Methods
    this.tackle = function(target){
            console.log(this.name + ' tackled ' + target.name);
            target.health -= this.attack;
            console.log(target.name + "'s health is now reduced to " + target.health);
            if(target.health <= 0) {
            console.log(target.name + ' fainted.');
        }
    }; 


    this.faint = function(target){
        console.log(target.name + ' fainted.')
    }
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 12);
let snorlax = new Pokemon('Snorlax', 8);
let eevee = new Pokemon("Eevee", 100);
let bulbasaur = new Pokemon('Bulbasaur', 24);


console.log(pikachu);
console.log(snorlax);
console.log(eevee);
console.log(bulbasaur);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
snorlax.tackle(pikachu);

bulbasaur.tackle(pikachu);

